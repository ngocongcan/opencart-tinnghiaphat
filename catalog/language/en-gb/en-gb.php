<?php
// Locale
$_['code']                  = 'en';
$_['direction']             = 'ltr';
$_['date_format_short']     = 'd/m/Y';
$_['date_format_long']      = 'l dS F Y';
$_['time_format']           = 'h:i:s A';
$_['datetime_format']       = 'd/m/Y H:i:s';
$_['decimal_point']         = '.';
$_['thousand_point']        = ',';

// Text
$_['text_home']             = '<i class="fa fa-home"></i>';
$_['text_yes']              = 'Có';
$_['text_no']               = 'Không';
$_['text_none']             = ' --- None --- ';
$_['text_select']           = ' --- Vui lòng chọn --- ';
$_['text_all_zones']        = 'Tất cả các khu';
$_['text_pagination']       = 'Hiện thị %d đến %d của %d (%d Trang)';
$_['text_loading']          = 'Đang tải...';
$_['text_no_results']       = 'Không có kết quả!';

// Buttons
$_['button_address_add']    = 'Thêm Địa chỉ';
$_['button_back']           = 'Trở về';
$_['button_continue']       = 'Tiếp tục';
$_['button_cart']           = 'Thêm vào giỏ hàng';
$_['button_cancel']         = 'Huỷ bỏ';
$_['button_compare']        = 'So sánh sản phẩm này';
$_['button_wishlist']       = 'Thêm vào danh sách mong muốn';
$_['button_checkout']       = 'Thanh toán';
$_['button_confirm']        = 'Xác nhận đơn hàng';
$_['button_coupon']         = 'Sử dụng mã khuyến mại';
$_['button_delete']         = 'Xoá';
$_['button_download']       = 'Tải về';
$_['button_edit']           = 'Chỉnh sửa';
$_['button_filter']         = 'Lọc tìm kiếm';
$_['button_new_address']    = 'Địa chỉ mới';
$_['button_change_address'] = 'Thay đổi Địa chỉ';
$_['button_reviews']        = 'Nhận xét';
$_['button_write']          = 'Viết Nhận xét';
$_['button_login']          = 'Đăng nhập';
$_['button_update']         = 'Cập nhật';
$_['button_remove']         = 'Xoá';
$_['button_reorder']        = 'Sắp xếp lại';
$_['button_return']         = 'Trở về';
$_['button_shopping']       = 'Tiếp tục mua hàng';
$_['button_search']         = 'Tìm kiếm';
$_['button_shipping']       = 'Áp dụng Vận Chuyển';
$_['button_submit']         = 'Gởi';
$_['button_guest']          = 'Khách thanh toán';
$_['button_view']           = 'Xem';
$_['button_voucher']        = 'Áp dụng chứng nhận quà tặng';
$_['button_upload']         = 'Tải tập tin lên';
$_['button_reward']         = 'Áp dụng điểm thưởng';
$_['button_quote']          = 'Nhận báo giá';
$_['button_list']           = 'Danh sách';
$_['button_grid']           = 'Lưới';
$_['button_map']            = 'Xem Google Map';

// Error
$_['error_exception']       = 'Mã lỗi(%s): %s ở %s trên dòng %s';
$_['error_upload_1']        = 'Cảnh báo: Các tập tin được tải lên vượt quá chỉ post_max_filesize trong php.ini!';
$_['error_upload_2']        = 'Cảnh báo: Các tập tin được tải lên vượt quá chỉ MAX_FILE_SIZE đã được quy định trong các dạng HTML!';
$_['error_upload_3']        = 'Cảnh báo: Các tập tin được tải lên chỉ tải lên được một phần!';
$_['error_upload_4']        = 'Cảnh báo: Không có tập tin đã được tải lên!';
$_['error_upload_6']        = 'Cảnh báo: Thiếu một thư mục tạm thời!';
$_['error_upload_7']        = 'Cảnh báo: Không thể ghi file vào đĩa!';
$_['error_upload_8']        = 'Cảnh báo: Tập tin tải lên dừng lại bằng cách mở rộng!';
$_['error_upload_999']      = 'Cảnh báo: Không có mã lỗi có sẵn!';
$_['error_curl']            = 'CURL: mã lỗi(%s): %s';
