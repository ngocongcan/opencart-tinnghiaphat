<?php
// Heading
$_['heading_title']     = 'So sánh sản phẩm';

// Text
$_['text_product']      = 'Thông tin chi tiết sản phẩm';
$_['text_name']         = 'Sản phẩm';
$_['text_image']        = 'Hình ảnh';
$_['text_price']        = 'Giá bán';
$_['text_model']        = 'Mẫu';
$_['text_manufacturer'] = 'Nhãn hiệu';
$_['text_availability'] = 'Khả dụng';
$_['text_instock']      = 'Trong kho';
$_['text_rating']       = 'Xếp hạng';
$_['text_reviews']      = 'Dựa trên %s đánh giá.';
$_['text_summary']      = 'Tóm lược';
$_['text_weight']       = 'Cân nặng';
$_['text_dimension']    = 'Kích thước (L x W x H)';
$_['text_compare']      = 'So sánh sản phẩm (%s)';
$_['text_success']      = 'Thành công: Bạn đã thêm <a href="%s">%s</a> của bạn <a href="%s"> so sánh sản phẩm</a>!';
$_['text_remove']       = 'Thành công: Bạn đã sửa đổi sản phẩm so sánh của bạn!';
$_['text_empty']        = 'Bạn chưa chọn bất kỳ sản phẩm để so sánh.';