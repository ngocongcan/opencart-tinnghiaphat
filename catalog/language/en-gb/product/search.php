<?php
// Heading
$_['heading_title']     = 'Tìm kiếm';
$_['heading_tag']		= 'Nhãn - ';

// Text
$_['text_search']       = 'Sản phẩm đáp ứng các tiêu chí tìm kiếm';
$_['text_keyword']      = 'Từ khóa';
$_['text_category']     = 'Tất cả danh mục';
$_['text_sub_category'] = 'Tìm trong danh mục';
$_['text_empty']        = 'Không có sản phẩm phù hợp với tiêu chí tìm kiếm.';
$_['text_quantity']     = 'Số lượng:';
$_['text_manufacturer'] = 'Nhãn hiệu:';
$_['text_model']        = 'Mã sản phẩm:';
$_['text_points']       = 'Điểm thưởng:';
$_['text_price']        = 'Giá bán:';
$_['text_tax']          = 'Thuế suất:';
$_['text_compare']      = 'So sánh sản phẩm (%s)';
$_['text_sort']         = 'Sắp xếp theo:';
$_['text_default']      = 'Mặc định';
$_['text_name_asc']     = 'Tên (A - Z)';
$_['text_name_desc']    = 'Tên (Z - A)';
$_['text_price_asc']    = 'Giá bán (Thấp &gt; Cao)';
$_['text_price_desc']   = 'Giá bán (Cao &gt; Thấp)';
$_['text_rating_asc']   = 'Xếp hạng (Thấp nhất)';
$_['text_rating_desc']  = 'Xếp hạng (Cao nhất)';
$_['text_model_asc']    = 'Mẫu (A - Z)';
$_['text_model_desc']   = 'Mẫu (Z - A)';
$_['text_limit']        = 'Hiển thị:';

// Entry
$_['entry_search']      = 'Tiêu chí tìm kiếm';
$_['entry_description'] = 'Tìm trong mô tả sản phẩm';