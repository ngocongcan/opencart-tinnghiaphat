<?php
// Text
$_['text_search']              = 'Tìm kiếm';
$_['text_brand']               = 'Nhãn hiệu';
$_['text_manufacturer']        = 'Nhãn hiệu:';
$_['text_model']               = 'Mã sản phẩm:';
$_['text_reward']              = 'Điểm thưởng:';
$_['text_points']              = 'Giá tại các điểm thưởng:';
$_['text_stock']               = 'Có sẵn:';
$_['text_instock']             = 'Trong kho';
$_['text_tax']                 = 'Thuế suất:';
$_['text_discount']            = ' hoặc nhiều hơn ';
$_['text_option']              = 'Tùy chọn có sẵn';
$_['text_minimum']             = 'Sản phẩm này có số lượng tối thiểu %s';
$_['text_reviews']             = '%s đánh giá';
$_['text_write']               = 'Viết đánh giá';
$_['text_login']               = 'Xin vui lòng <a href="%s">đăng nhập</a> hoặc là <a href="%s">ghi danh</a> xem lại';
$_['text_no_reviews']          = 'Không có đánh giá nào về sản phẩm này.';
$_['text_note']                = '<span class="text-danger">Chú thích:</span> không hỗ trợ HTML!';
$_['text_success']             = 'Cám ơn, vì phản hồi của bạn. Nó đã được nộp cho quản trị trang web chính.';
$_['text_related']             = 'Những sản phẩm tương tự';
$_['text_tags']                = 'Thẻ:';
$_['text_error']               = 'Sản phẩm không có!';
$_['text_payment_recurring']   = 'Hồ sơ thanh toán';
$_['text_trial_description']   = '%s mỗi %d %s(s) cho %d thanh toán sau';
$_['text_payment_description'] = '%s mỗi %d %s(s) cho %d thanh toán';
$_['text_payment_cancel']      = '%s mỗi %d %s(s) cho đến khi bị hủy bỏ';
$_['text_day']                 = 'ngày';
$_['text_week']                = 'tuần';
$_['text_semi_month']          = 'nửa tháng';
$_['text_month']               = 'tháng';
$_['text_year']                = 'năm';

// Entry
$_['entry_qty']                = 'Số lượng';
$_['entry_name']               = 'Tên của bạn';
$_['entry_review']             = 'Đánh giá của bạn';
$_['entry_rating']             = 'Xếp hạng';
$_['entry_good']               = 'Tốt';
$_['entry_bad']                = 'Xấu';

// Tabs
$_['tab_description']          = 'Miêu tả';
$_['tab_attribute']            = 'Đặc điểm kỹ thuật';
$_['tab_review']               = 'Nhận xét (%s)';

// Error
$_['error_name']               = 'Cảnh báo: Tên phải có từ 3 đến 25 ký tự!';
$_['error_text']               = 'Cảnh báo: Nội dung phải có từ 25 đến 1000 ký tự!';
$_['error_rating']             = 'Cảnh báo: Vui lòng chọn một đánh giá xem xét lại!';
