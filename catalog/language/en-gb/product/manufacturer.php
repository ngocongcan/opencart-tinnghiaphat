<?php
// Heading
$_['heading_title']     = 'Tìm thương hiệu yêu thích của bạn';

// Text
$_['text_brand']        = 'Nhãn hiệu';
$_['text_index']        = 'Chỉ số hiệu:';
$_['text_error']        = 'Nhãn hiệu không được tìm thấy!';
$_['text_empty']        = 'Không có sản phẩm nào.';
$_['text_quantity']     = 'Số lượng:';
$_['text_manufacturer'] = 'Nhãn hiệu:';
$_['text_model']        = 'Mã sản phẩm:';
$_['text_points']       = 'Điểm thưởng:';
$_['text_price']        = 'Giá bán:';
$_['text_tax']          = 'Thuế suất:';
$_['text_compare']      = 'So sánh sản phẩm (%s)';
$_['text_sort']         = 'Sắp xếp theo:';
$_['text_default']      = 'Mặc định';
$_['text_name_asc']     = 'Tên (A - Z)';
$_['text_name_desc']    = 'Tên (Z - A)';
$_['text_price_asc']    = 'Giá bán (Thấp &gt; Cao)';
$_['text_price_desc']   = 'Giá bán (Cao &gt; Thấp)';
$_['text_rating_asc']   = 'Xếp hạng (Thấp nhất)';
$_['text_rating_desc']  = 'Xếp hạng (Cao nhất)';
$_['text_model_asc']    = 'Mẫu (A - Z)';
$_['text_model_desc']   = 'Mẫu (Z - A)';
$_['text_limit']        = 'Hiển thị:';
