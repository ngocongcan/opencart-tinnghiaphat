<?php
// Text
$_['text_success']     = 'Thành công: giảm giá phiếu quà tặng của bạn đã được áp dụng!';
$_['text_cart']        = 'Thành công: Bạn đã sửa đổi giỏ hàng của bạn!';
$_['text_for']         = '%s Giấy chứng nhận quà tặng cho %s';

// Error
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền truy cập vào các API!';
$_['error_voucher']    = 'Cảnh báo: Gift Voucher là không hợp lệ hoặc sự cân bằng đã được sử dụng hết!';
$_['error_to_name']    = 'Tên người nhận phải có từ 1 đến 64 ký tự!';
$_['error_from_name']  = 'Tên của bạn phải có từ 1 đến 64 ký tự!';
$_['error_email']      = 'E-Mail Địa chỉ không có vẻ hợp lệ!';
$_['error_theme']      = 'Bạn phải chọn một chủ đề!';
$_['error_amount']     = 'Số tiền phải từ %s và %s!';
