<?php
// Heading
$_['heading_title']             = 'Phương thức thanh toán';

// Text
$_['text_account']              = 'Tài khoản';
$_['text_payment']              = 'Thanh toán';
$_['text_your_payment']         = 'Thông tin thanh toán';
$_['text_your_password']        = 'Mật khẩu của bạn';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Chuyển khoản ngân hàng';
$_['text_success']              = 'Thành công: Tài khoản của bạn đã được cập nhật thành công.';

// Entry
$_['entry_tax']                 = 'Mã số thuế';
$_['entry_payment']             = 'Phương thức thanh toán';
$_['entry_cheque']              = 'Tên người thụ hưởng séc';
$_['entry_paypal']              = 'Tài khoản Email PayPal';
$_['entry_bank_name']           = 'Tên ngân hàng';
$_['entry_bank_branch_number']  = 'Số ABA / BSB (Chi nhánh số)';
$_['entry_bank_swift_code']     = 'SWIFT Code';
$_['entry_bank_account_name']   = 'Tên tài khoản';
$_['entry_bank_account_number'] = 'Số tài khoản';