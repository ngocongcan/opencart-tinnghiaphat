<?php
// Heading
$_['heading_title'] = 'Tài khoản liên kết của bạn đã được tạo!';

// Text
$_['text_message']  = '<p>Xin chúc mừng! tài khoản mới của bạn đã được tạo thành công!</p> <p>Bạn đã là thành viên của %s chi nhánh.</p> <p>Nếu bạn có bất kỳ câu hỏi về hoạt động của hệ thống liên kết này, xin vui lòng e-mail cho các chủ cửa hàng.</p> <p>Một xác nhận đã được gửi đến địa chỉ e-mail cung cấp. Nếu bạn không nhận được nó trong vòng một giờ, xin vui lòng <a href="%s">liên hệ chúng tôi</a>.</p>';
$_['text_approval'] = '<p>Cảm ơn bạn đã đăng ký một tài khoản liên kết với %s!</p><p>Bạn sẽ được thông báo bằng e-mail khi tài khoản của bạn đã được kích hoạt bởi các chủ cửa hàng.</p><p>Nếu bạn có bất kỳ câu hỏi về hoạt động của hệ thống liên kết này, xin vui lòng <a href="%s">liên hệ với chủ cửa hàng</a>.</p>';
$_['text_account']  = 'Tài khoản';
$_['text_success']  = 'Thành công';
