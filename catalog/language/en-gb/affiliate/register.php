<?php
// Heading
$_['heading_title']             = 'Chương trình liên kết';

// Text
$_['text_account']              = 'Tài khoản';
$_['text_register']             = 'Đăng ký liên kết';
$_['text_account_already']      = 'Nếu bạn đã có một tài khoản với chúng tôi, xin vui lòng đăng nhập tại <a href="%s">trang đăng nhập</a>.';
$_['text_signup']               = 'Để tạo một tài khoản liên kết, điền vào mẫu dưới đây đảm bảo bạn hoàn thành tất cả các lĩnh vực cần thiết:';
$_['text_your_details']         = 'Thông tin cá nhân của bạn';
$_['text_your_address']         = 'Chi tiết địa chỉ của bạn';
$_['text_payment']              = 'Thông tin thanh toán';
$_['text_your_password']        = 'Mật khẩu của bạn';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Chuyển khoản ngân hàng';
$_['text_agree']                = 'Tôi đã đọc và đồng ý với <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_firstname']           = 'Tên';
$_['entry_lastname']            = 'Họ';
$_['entry_email']               = 'E-Mail';
$_['entry_telephone']           = 'Điện thoại';
$_['entry_fax']                 = 'Fax';
$_['entry_company']             = 'Công ty';
$_['entry_website']             = 'Địa chỉ trang web';
$_['entry_address_1']           = 'Địa chỉ 1';
$_['entry_address_2']           = 'Địa chỉ 2';
$_['entry_postcode']            = 'Mã bưu';
$_['entry_city']                = 'Thành phố';
$_['entry_country']             = 'Quốc gia';
$_['entry_zone']                = 'Khu vực / Nhà nước';
$_['entry_tax']                 = 'Mã số thuế';
$_['entry_payment']             = 'Phương thức thanh toán';
$_['entry_cheque']              = 'Tên người thụ hưởng séc';
$_['entry_paypal']              = 'Tài khoản email PayPal';
$_['entry_bank_name']           = 'Tên ngân hàng';
$_['entry_bank_branch_number']  = 'Số ABA / BSB (Chi nhánh số)';
$_['entry_bank_swift_code']     = 'SWIFT Code';
$_['entry_bank_account_name']   = 'Tên tài khoản';
$_['entry_bank_account_number'] = 'Số tài khoản';
$_['entry_password']            = 'Mật khẩu';
$_['entry_confirm']             = 'Xác nhận mật khẩu';

// Error
$_['error_exists']              = 'Cảnh báo: Địa chỉ E-Mail đã được đăng ký!';
$_['error_firstname']           = 'Tên phải được từ 1 đến 32 ký tự!';
$_['error_lastname']            = 'Họ phải có từ 1 đến 32 ký tự!';
$_['error_email']               = 'Địa chỉ E-Mail không có vẻ hợp lệ!';
$_['error_telephone']           = 'Điện thoại phải có từ 3 đến 32 ký tự!';
$_['error_password']            = 'Mật khẩu phải từ 4 đến 20 ký tự!';
$_['error_confirm']             = 'Mật khẩu xác nhận không khớp mật khẩu!';
$_['error_address_1']           = 'Địa chỉ 1 phải có từ 3 đến 128 ký tự!';
$_['error_city']                = 'Thành phố phải có từ 2 và 128 ký tự!';
$_['error_country']             = 'Vui lòng chọn một quốc gia!';
$_['error_zone']                = 'Vui lòng chọn một khu vực / tiểu bang!';
$_['error_postcode']            = 'Mã bưu phải có từ 2 đến 10 ký tự!';
$_['error_agree']               = 'Cảnh báo: Bạn phải đồng ý với %s!';