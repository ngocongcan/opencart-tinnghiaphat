<?php
// Heading
$_['heading_title'] = 'Thoát tài khoản';

// Text
$_['text_message']  = '<p>Bạn đã được đăng xuất tài khoản liên kết của bạn.</p>';
$_['text_account']  = 'Tài khoản';
$_['text_logout']   = 'Đăng xuất';