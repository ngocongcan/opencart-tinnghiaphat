<?php
// Heading
$_['heading_title']        = 'Tài khoản liên kết của tôi';

// Text
$_['text_account']         = 'Tài khoản';
$_['text_my_account']      = 'Tài khoản liên kết của tôi';
$_['text_my_tracking']     = 'Thông tin theo dõi của tôi';
$_['text_my_transactions'] = 'Giao dịch của tôi';
$_['text_edit']            = 'Chỉnh sửa thông tin tài khoản của bạn';
$_['text_password']        = 'Thay đổi mật khẩu của bạn';
$_['text_payment']         = 'Thay đổi tùy chọn thanh toán của bạn';
$_['text_tracking']        = 'Tuỳ chỉnh liên kết mã theo dõi';
$_['text_transaction']     = 'Xem lịch sử giao dịch của bạn';