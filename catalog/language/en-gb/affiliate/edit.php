<?php
// Heading
$_['heading_title']     = 'Thông tin Tài khoản của tôi';

// Text
$_['text_account']      = 'Tài khoản';
$_['text_edit']         = 'Chỉnh sửa thông tin';
$_['text_your_details'] = 'Thông tin cá nhân của bạn';
$_['text_your_address'] = 'Địa chỉ của bạn';
$_['text_success']      = 'Thành công: tài khoản của bạn đã được cập nhật thành công.';

// Entry
$_['entry_firstname']   = 'Tên';
$_['entry_lastname']    = 'Họ';
$_['entry_email']       = 'E-Mail';
$_['entry_telephone']   = 'Điện thoại';
$_['entry_fax']         = 'Fax';
$_['entry_company']     = 'Công ty';
$_['entry_website']     = 'Địa chỉ trang web';
$_['entry_address_1']      = 'Địa chỉ nhà 1';
$_['entry_address_2']      = 'Địa chỉ nhà 2';
$_['entry_postcode']       = 'Mã bưu điện';
$_['entry_city']           = 'Thành phố';
$_['entry_country']        = 'Quốc gia';
$_['entry_zone']           = 'Khu vực / Nhà nước';
$_['entry_default']        = 'Địa chỉ mặc định';

// Error
$_['error_exists']      = 'Cảnh báo: Địa chỉ E-Mail đã được đăng ký!';
$_['error_firstname']   = 'Tên phải được từ 1 đến 32 ký tự!';
$_['error_lastname']    = 'Họ phải có từ 1 đến 32 ký tự!';
$_['error_email']       = 'Địa chỉ E-Mail không có vẻ hợp lệ!';
$_['error_telephone']   = 'Điện thoại phải có từ 3 đến 32 ký tự!';
$_['error_address_1']   = 'Địa chỉ 1 phải có từ 3 đến 128 ký tự!';
$_['error_city']        = 'Thành phố phải có từ 2 và 128 ký tự!';
$_['error_country']     = 'Vui lòng chọn một quốc gia!';
$_['error_zone']        = 'Vui lòng chọn một khu vực / tiểu bang!';
$_['error_postcode']    = 'Mã bưu phải có từ 2 đến 10 ký tự!';