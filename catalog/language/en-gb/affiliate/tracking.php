<?php
// Heading
$_['heading_title']    = 'Theo dõi liên kết';

// Text
$_['text_account']     = 'Tài khoản';
$_['text_description'] = 'Để đảm bảo bạn được trả tiền cho các giới thiệu bạn gửi cho chúng tôi, chúng tôi cần phải theo dõi giới thiệu bằng cách đặt một mã theo dõi trong liên kết của URL cho chúng ta. Bạn có thể sử dụng các công cụ dưới đây để tạo ra các liên kết đến trang web của %s.';

// Entry
$_['entry_code']       = 'Mã theo dõi của bạn';
$_['entry_generator']  = 'Theo dõi liên kết';
$_['entry_link']       = 'Liên kết theo dõi';

// Help
$_['help_generator']  = 'Gõ vào tên của một sản phẩm mà bạn muốn liên kết đến';