<?php
// Heading
$_['heading_title']                 = 'Chương trình liên kết';

// Text
$_['text_account']                  = 'Tài khoản';
$_['text_login']                    = 'Đăng nhập';
$_['text_description']              = '<p>%s chương trình liên kết là miễn phí và cho phép thành viên để kiếm thêm thu nhập bằng cách đặt một liên kết hoặc liên kết trên trang web của họ mà quảng cáo% s hoặc các sản phẩm cụ thể về nó. Bất kỳ doanh thu thực hiện cho khách hàng đã nhấp vào các liên kết này sẽ kiếm được hoa hồng liên kết. Tỷ lệ hoa hồng tiêu chuẩn hiện nay là %s.</p><p>Để biết thêm thông tin, hãy truy cập trang FAQ của chúng tôi hoặc xem các điều khoản liên kết của chúng tôi &amp; điều kiện.</p>';
$_['text_new_affiliate']            = 'New Affiliate';
$_['text_register_account']         = '<p>Tôi hiện không phải là liên kết.</p><p>Nhấp vào Tiếp tục dưới đây để tạo một tài khoản liên kết mới. Xin lưu ý rằng điều này không được kết nối theo cách nào để tài khoản khách hàng của bạn.</p>';
$_['text_returning_affiliate']      = 'Đăng nhập liên kết';
$_['text_i_am_returning_affiliate'] = 'Tôi là một liên kết trở lại.';
$_['text_forgotten']                = 'Đã quên mật khẩu';

// Entry
$_['entry_email']                   = 'E-Mail liên kết';
$_['entry_password']                = 'Mật khẩu';

// Error
$_['error_login']                   = 'Cảnh báo: Không phù hợp cho Địa chỉ E-Mail và/hoặc mật khẩu.';
$_['error_attempts']                = 'Cảnh báo: Tài khoản của bạn đã vượt quá số lượng cho phép của nỗ lực đăng nhập. Vui lòng thử lại trong 1 giờ.';
$_['error_approved']                = 'Cảnh báo: Tài khoản của bạn đòi hỏi phải có sự chấp thuận trước khi bạn có thể đăng nhập.';