<?php
// Text
$_['text_home']          = 'Trang chủ';
$_['text_wishlist']      = 'Danh sách mong muốn (%s)';
$_['text_shopping_cart'] = 'Giỏ hàng';
$_['text_category']      = 'Thể loại';
$_['text_account']       = 'Tài khoản của tôi';
$_['text_register']      = 'Đăng kí';
$_['text_login']         = 'Đăng nhập';
$_['text_order']         = 'Lịch sử đơn hàng';
$_['text_transaction']   = 'Giao dịch';
$_['text_download']      = 'Tải về';
$_['text_logout']        = 'Đăng xuất';
$_['text_checkout']      = 'Thanh toán';
$_['text_search']        = 'Tìm kiếm';
$_['text_all']           = 'Hiển thị tất cả';
