<?php
// Text
$_['text_information']  = 'Thông tin';
$_['text_service']      = 'Dịch vụ khách hàng';
$_['text_extra']        = 'Thêm';
$_['text_contact']      = 'Liên hệ chúng tôi';
$_['text_return']       = 'Trả lại';
$_['text_sitemap']      = 'Sơ đồ trang web';
$_['text_manufacturer'] = 'Nhãn hiệu';
$_['text_voucher']      = 'Phiếu quà tặng';
$_['text_affiliate']    = 'Đại lý';
$_['text_special']      = 'Đặc biệt';
$_['text_account']      = 'Tài khoản của tôi';
$_['text_order']        = 'Lịch sử đơn hàng';
$_['text_wishlist']     = 'Danh sách mong muốn';
$_['text_newsletter']   = 'Bản tin';
$_['text_powered']      = 'Thiết kế bởi <a href="#">Vicnisoft </a><br /> %s &copy; %s';
