<?php
// Heading
$_['heading_title'] = 'Thanh toán thất bại!';

// Text
$_['text_basket']   = 'Giỏ hàng';
$_['text_checkout'] = 'Thanh toán';
$_['text_failure']  = 'Thanh toán thất bại';
$_['text_message']  = '<p>Có một vấn đề xử lý thanh toán của bạn và thứ tự không hoàn thành.</p>

<p>Lý do có thể là:</p>
<ul>
  <li>Không đủ tiền</li>
  <li>Xác minh không hoàn thành</li>
</ul>

<p>Hãy cố gắng để đặt hàng một lần nữa bằng cách sử dụng phương thức thanh toán khác nhau.</p>

<p>Nếu vấn đề vẫn xin vui lòng <a href="%s">liên hệ chúng tôi</a> với các chi tiết đặt hàng bạn đang cố gắng đặt.</p>
';
