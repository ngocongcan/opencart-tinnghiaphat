<?php
// Heading
$_['heading_title']                  = 'Thanh toán';

// Text
$_['text_cart']                      = 'Giỏ hàng';
$_['text_checkout_option']           = 'Bước %s: Tùy chọn Checkout';
$_['text_checkout_account']          = 'Bước %s: Tài khoản &amp; Chi tiết Thanh toán';
$_['text_checkout_payment_address']  = 'Bước %s: Chi tiết Thanh toán';
$_['text_checkout_shipping_address'] = 'Bước %s: Chi tiết giao hàng';
$_['text_checkout_shipping_method']  = 'Bước %s: Phương thức vận chuyển';
$_['text_checkout_payment_method']   = 'Bước %s: Phương thức thanh toán';
$_['text_checkout_confirm']          = 'Bước %s: Xác nhận đơn hàng';
$_['text_modify']                    = 'Sửa đổi &raquo;';
$_['text_new_customer']              = 'Khách hàng mới';
$_['text_returning_customer']        = 'hách hàng cũ';
$_['text_checkout']                  = 'Tùy chọn thanh toán:';
$_['text_i_am_returning_customer']   = 'Tôi là một khách hàng cũ';
$_['text_register']                  = 'Đăng ký tài khoản';
$_['text_guest']                     = 'Khách thanh toán';
$_['text_register_account']          = 'Bằng cách tạo một tài khoản bạn sẽ có thể mua sắm nhanh hơn, cập nhật trạng thái đơn hàng, và theo dõi các đơn hàng bạn đã thực hiện trước đây.';
$_['text_forgotten']                 = 'Quên mật khẩu';
$_['text_your_details']              = 'Thông tin cá nhân của bạn';
$_['text_your_address']              = 'Địa chỉ của bạn';
$_['text_your_password']             = 'Mật khẩu của bạn';
$_['text_agree']                     = 'Tôi đã đọc và đồng ý với <a href="%s" class="agree"><b>%s</b></a>';
$_['text_address_new']               = 'Tôi muốn sử dụng một địa chỉ mới';
$_['text_address_existing']          = 'Tôi muốn sử dụng một địa chỉ hiện tại';
$_['text_shipping_method']           = 'Vui lòng chọn phương thức giao hàng ưa thích sử dụng đơn đặt hàng này.';
$_['text_payment_method']            = 'Vui lòng chọn phương thức thanh toán ưa thích để sử dụng trên đơn hàng này.';
$_['text_comments']                  = 'Thêm bình luận về đơn hàng của bạn';
$_['text_recurring_item']            = 'Hàng định kỳ';
$_['text_payment_recurring']         = 'Hồ sơ thanh toán';
$_['text_trial_description']         = '%s mỗi %d %s(s) cho %d thanh toán sau';
$_['text_payment_description']       = '%s mỗi %d %s(s) cho %d thanh toán';
$_['text_payment_cancel']            = '%s mỗi %d %s(s) cho đến khi bị hủy bỏ';
$_['text_day']                       = 'ngày';
$_['text_week']                      = 'tuần';
$_['text_semi_month']                = 'nửa tháng';
$_['text_month']                     = 'tháng';
$_['text_year']                      = 'năm';

// Column
$_['column_name']                    = 'Tên sản phẩm';
$_['column_model']                   = 'Mô hình';
$_['column_quantity']                = 'Số lượng';
$_['column_price']                   = 'Đơn giá';
$_['column_total']                   = 'Tổng số';

// Entry
$_['entry_email_address']            = 'Địa chỉ email';
$_['entry_email']                    = 'E-Mail';
$_['entry_password']                 = 'Mật khẩu';
$_['entry_confirm']                  = 'Xác nhận mật khẩu';
$_['entry_firstname']                = 'Tên';
$_['entry_lastname']                 = 'Họ';
$_['entry_telephone']                = 'Điện thoại';
$_['entry_fax']                      = 'Fax';
$_['entry_address']                  = 'Chọn Địa chỉ';
$_['entry_company']                  = 'Công ty';
$_['entry_customer_group']           = 'Nhóm khách hàng';
$_['entry_address_1']                = 'Địa chỉ 1';
$_['entry_address_2']                = 'Địa chỉ 2';
$_['entry_postcode']                 = 'Mã bưu điện';
$_['entry_city']                     = 'Thành phố';
$_['entry_country']                  = 'Quốc gia';
$_['entry_zone']                     = 'Khu vực / Nhà nước';
$_['entry_newsletter']               = 'Tôi muốn đăng ký vào các %s bản tin.';
$_['entry_shipping'] 	             = 'Địa chỉ giao hàng và thanh toán của tôi đều giống nhau.';

// Error
$_['error_warning']                  = 'Có một vấn đề trong khi cố gắng để xử lý đơn đặt hàng! Nếu vấn đề vẫn còn hãy thử chọn một phương thức thanh toán khác nhau hoặc bạn có thể liên hệ với chủ cửa hàng bởi <a href="%s">nhấn vào đây</a>.';
$_['error_login']                    = 'Cảnh báo: Không phù hợp cho Địa chỉ E-Mail và/hoặc mật khẩu.';
$_['error_attempts']                 = 'Cảnh báo: Tài khoản của bạn đã vượt quá số lượng cho phép của nỗ lực đăng nhập. Vui lòng thử lại trong 1 giờ.';
$_['error_approved']                 = 'Cảnh báo: Tài khoản của bạn đòi hỏi phải có sự chấp thuận trước khi bạn có thể đăng nhập.';
$_['error_exists']                   = 'Cảnh báo: E-Mail Địa chỉ đã được đăng ký!';
$_['error_firstname']                = 'Tên phải được từ 1 đến 32 ký tự!';
$_['error_lastname']                 = 'Họ phải có từ 1 đến 32 ký tự!';
$_['error_email']                    = 'Địa chỉ E-Mail không có vẻ hợp lệ!';
$_['error_telephone']                = 'Điện thoại phải có từ 3 đến 32 ký tự!';
$_['error_password']                 = 'Mật khẩu phải từ 4 đến 20 ký tự!';
$_['error_confirm']                  = 'Mật khẩu xác nhận không khớp mật khẩu!';
$_['error_address_1']                = 'Địa chỉ 1 phải có từ 3 đến 128 ký tự!';
$_['error_city']                     = 'Thành phố phải có từ 2 và 128 ký tự!';
$_['error_postcode']                 = 'Mã bưu phải có từ 2 đến 10 ký tự!';
$_['error_country']                  = 'Vui lòng chọn một quốc gia!';
$_['error_zone']                     = 'Vui lòng chọn một vùng / nhà nước!';
$_['error_agree']                    = 'Cảnh báo: Bạn phải đồng ý với %s!';
$_['error_address']                  = 'Cảnh báo: Bạn phải chọn địa chỉ!';
$_['error_shipping']                 = 'Cảnh báo: Phương thức vận chuyển là bắt buộc!';
$_['error_no_shipping']              = 'Cảnh báo: Không có tùy chọn Vận chuyển có sẵn. Xin vui lòng <a href="%s">liên hệ chúng tôi</a> để được hỗ trợ!';
$_['error_payment']                  = 'Cảnh báo: Phương thức thanh toán là bắt buộc';
$_['error_no_payment']               = 'Cảnh báo: Không có tùy chọn thanh toán có sẵn. Xin vui lòng <a href="%s">liên hệ chúng tôi</a> để được hỗ trợ!';
$_['error_custom_field']             = '%s là bắt buộc!';