<?php
// Heading
$_['heading_title']        = 'Đơn hàng của bạn đã được đặt!';

// Text
$_['text_basket']          = 'Giỏ hàng';
$_['text_checkout']        = 'Thanh toán';
$_['text_success']         = 'Thành công';
$_['text_customer']        = '<p>Đơn hàng của bạn đã được xử lý thành công!</p><p>Bạn có thể xem lịch sử đặt hàng của bạn bằng cách vào <a href="%s">trang tài khoản của tôi</a>  và bằng cách nhấp vào <a href="%s">lịch sử</a>.</p><p>Nếu mua hàng của bạn có tải về liên kết, bạn có thể vào tài khoản <a href="%s">trang tải</a> để xem chúng.</p><p>Hãy liên hệ với <a href="%s">chủ cửa hàng</a> nếu bạn có bất kì câu hỏi nào.</p><p>Cảm ơn đã mua sắm với chúng tôi trực tuyến!</p>';
$_['text_guest']           = '<p>Đơn hàng của bạn đã được xử lý thành công!</p><p>Hãy liên hệ với <a href="%s">chủ cửa hàng nếu bạn có bất kì câu hỏi nào.</a>.</p><p>Cảm ơn đã mua sắm với chúng tôi trực tuyến!</p>';