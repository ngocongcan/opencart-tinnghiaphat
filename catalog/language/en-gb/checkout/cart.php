<?php
// Heading
$_['heading_title']            = 'Giỏ hàng';

// Text
$_['text_success']             = 'Thành công: Bạn đã thêm <a href="%s">%s</a> vào <a href="%s">giỏ hàng của bạn</a>!';
$_['text_remove']              = 'Thành công: Bạn đã sửa đổi giỏ hàng của bạn!';
$_['text_login']               = 'Chú ý: Bạn phải <a href="%s">đăng nhập</a> or <a href="%s">tạo một tài khoản</a> để xem giá!';
$_['text_items']               = '%s sản phẩm - %s';
$_['text_points']              = 'Điểm thưởng: %s';
$_['text_next']                = 'Bạn thích làm gì tiếp theo?';
$_['text_next_choice']         = 'Chọn nếu bạn có một mã giảm giá hoặc các điểm thưởng bạn muốn sử dụng hoặc muốn để ước tính chi phí giao hàng của bạn.';
$_['text_empty']               = 'Giỏ hàng của bạn đang trống!';
$_['text_day']                 = 'ngày';
$_['text_week']                = 'tuần';
$_['text_semi_month']          = 'nửa tháng';
$_['text_month']               = 'tháng';
$_['text_year']                = 'năm';
$_['text_trial']               = '%s mỗi %s %s trong %s thanh toán sau đó';
$_['text_recurring']           = '%s mỗi %s %s';
$_['text_length']              = 'cho %s thanh toán';
$_['text_until_cancelled']     = 'cho đến khi bị hủy bỏ';
$_['text_recurring_item']      = 'Định kỳ hàng';
$_['text_payment_recurring']   = 'Hồ sơ thanh toán';
$_['text_trial_description']   = '%s mỗi %d %s(s) cho %d thanh toán(s) sau';
$_['text_payment_description'] = '%s mỗi %d %s(s) cho %d thanh toán(s)';
$_['text_payment_cancel']      = '%s mỗi %d %s(s) cho đến khi bị hủy bỏ';

// Column
$_['column_image']             = 'Hình ảnh';
$_['column_name']              = 'Tên sản phẩm';
$_['column_model']             = 'Mẫu';
$_['column_quantity']          = 'Số lượng';
$_['column_price']             = 'Đơn giá';
$_['column_total']             = 'Tổng số';

// Error
$_['error_stock']              = 'Sản phẩm được đánh dấu bằng *** không có sẵn trong số lượng mong muốn hoặc không có trong kho!';
$_['error_minimum']            = 'số lượng đặt hàng tối thiểu cho %s là %s!';
$_['error_required']           = '%s cần thiết!';
$_['error_product']            = 'Cảnh báo: Không có sản phẩm trong giỏ hàng của bạn!';
$_['error_recurring_required'] = 'Vui lòng chọn thanh toán định kỳ!';