<?php
// Heading
$_['heading_title'] = 'Sản phẩm yêu thích';

// Text
$_['text_account']  = 'Tài khoản';
$_['text_instock']  = 'Trong kho';
$_['text_wishlist'] = 'Danh sách mong muốn (%s)';
$_['text_login']    = 'Bạn phải <a href="%s">đăng nhập</a> hoặc là <a href="%s">tạo một tài khoản</a> để lưu <a href="%s">%s</a> của bạn<a href="%s"> sản phẩm mong muốn</a>!';
$_['text_success']  = 'Thành công: Bạn đã thêm <a href="%s">%s</a> của bạn <a href="%s">sản phẩm mong muốn</a>!';
$_['text_remove']   = 'Thành công: Bạn đã sửa đổi danh sách mong muốn của bạn!';
$_['text_empty']    = 'Danh sách mong muốn của bạn đang trống.';

// Column
$_['column_image']  = 'Hình ảnh';
$_['column_name']   = 'Tên sản phẩm';
$_['column_model']  = 'Mẫu';
$_['column_stock']  = 'Cổ phần';
$_['column_price']  = 'Đơn giá';
$_['column_action'] = 'Hoạt động';