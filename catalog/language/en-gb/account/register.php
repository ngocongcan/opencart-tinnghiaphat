<?php
// Heading
$_['heading_title']        = 'Đăng ký tài khoản';

// Text
$_['text_account']         = 'Tài khoản';
$_['text_register']        = 'Ghi danh';
$_['text_account_already'] = 'Nếu bạn đã có một tài khoản với chúng tôi, xin vui lòng đăng nhập tại <a href="%s"> trang đăng nhập</a>.';
$_['text_your_details']    = 'Thông tin cá nhân của bạn';
$_['text_your_address']    = 'Địa chỉ của bạn';
$_['text_newsletter']      = 'Bản tin';
$_['text_your_password']   = 'Mật khẩu của bạn';
$_['text_agree']           = 'Tôi đã đọc và đồng ý với <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Nhóm khách hàng';
$_['entry_firstname']      = 'Tên';
$_['entry_lastname']       = 'Họ';
$_['entry_email']          = 'E-Mail';
$_['entry_telephone']      = 'Điện thoại';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Công ty';
$_['entry_address_1']      = 'Địa chỉ 1';
$_['entry_address_2']      = 'Địa chỉ 2';
$_['entry_postcode']       = 'Mã bưu điện';
$_['entry_city']           = 'Thành phố';
$_['entry_country']        = 'Quốc gia';
$_['entry_zone']           = 'Khu vực';
$_['entry_newsletter']     = 'Theo dõi';
$_['entry_password']       = 'Mật khẩu';
$_['entry_confirm']        = 'Xác nhận mật khẩu';

// Error
$_['error_exists']         = 'Cảnh báo: E-Mail Địa chỉ đã được đăng ký!';
$_['error_firstname']      = 'Tên phải được từ 1 đến 32 ký tự!';
$_['error_lastname']       = 'Họ phải có từ 1 đến 32 ký tự!';
$_['error_email']          = 'Địa chỉ E-Mail không có vẻ hợp lệ!';
$_['error_telephone']      = 'Điện thoại phải có từ 3 đến 32 ký tự!';
$_['error_address_1']      = 'Địa chỉ 1 phải có từ 3 đến 128 ký tự!';
$_['error_city']           = 'Thành phố phải có từ 2 và 128 ký tự!';
$_['error_postcode']       = 'Mã bưu phải có từ 2 đến 10 ký tự!';
$_['error_country']        = 'Vui lòng chọn một quốc gia!';
$_['error_zone']           = 'Vui lòng chọn một vùng / nhà nước!';
$_['error_custom_field']   = '%s cần thiết!';
$_['error_password']       = 'Mật khẩu phải từ 4 đến 20 ký tự!';
$_['error_confirm']        = 'Mật khẩu xác nhận không khớp mật khẩu!';
$_['error_agree']          = 'Cảnh báo: Bạn phải đồng ý với %s!';