<?php
// Heading
$_['heading_title'] = 'Tài khoản của bạn đã được tạo!';

// Text
$_['text_message']  = '<p>Xin chúc mừng! tài khoản mới của bạn đã được tạo thành công!</p> <p>Bây giờ bạn có thể tận dụng các đặc quyền thành viên để nâng cao kinh nghiệm mua sắm trực tuyến của bạn với chúng tôi.</p> <p>Nếu bạn có bất kỳ câu hỏi về hoạt động của cửa hàng trực tuyến này, xin vui lòng e-mail cho các chủ cửa hàng.</p> <p>Một xác nhận đã được gửi đến địa chỉ e-mail cung cấp. Nếu bạn không nhận được nó trong vòng một giờ, xin vui lòng <a href="%s">liên hệ chúng tôi</a>.</p>';
$_['text_approval'] = '<p>Cảm ơn bạn đã đăng ký với %s!</p><p>Bạn sẽ được thông báo bằng e-mail khi tài khoản của bạn đã được kích hoạt bởi các chủ cửa hàng.</p><p>Nếu bạn có bất kỳ câu hỏi về hoạt động của cửa hàng trực tuyến này, xin vui lòng <a href="%s">liên hệ với chủ cửa hàng</a>.</p>';
$_['text_account']  = 'Tài khoản';
$_['text_success']  = 'Thành công';
