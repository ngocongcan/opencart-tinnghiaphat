<?php
// Heading
$_['heading_title']      = 'Trả sản phẩm';

// Text
$_['text_account']       = 'Tài khoản';
$_['text_return']        = 'Thông tin trả lại';
$_['text_return_detail'] = 'Chi tiết trả lại';
$_['text_description']   = 'Vui lòng điền vào mẫu dưới đây để yêu cầu một số RMA.';
$_['text_order']         = 'Thông tin đặt hàng';
$_['text_product']       = 'Thông tin sản phẩm';
$_['text_reason']        = 'Lý do trả ';
$_['text_message']       = '<p>Cảm ơn bạn đã gửi yêu cầu của bạn trở lại. Yêu cầu của bạn đã được gửi đến các bộ phận có liên quan để xử lý.</p><p> Bạn sẽ được thông báo qua e-mail để tình trạng yêu cầu của bạn.</p>';
$_['text_return_id']     = 'Mã trả lại :';
$_['text_order_id']      = 'ID đơn hàng:';
$_['text_date_ordered']  = 'Ngày đặt hàng:';
$_['text_status']        = 'Trạng thái:';
$_['text_date_added']    = 'Ngày thêm:';
$_['text_comment']       = 'Chú thích';
$_['text_history']       = 'Lịch sử trả hàng';
$_['text_empty']         = 'Bạn chưa có lịch sử trả hàng!';
$_['text_agree']         = 'Tôi đã đọc và đồng ý với <a href="%s" class="agree"><b>%s</b></a>';

// Column
$_['column_return_id']   = 'ID trả hàng';
$_['column_order_id']    = 'ID đơn hàng';
$_['column_status']      = 'Trạng thái';
$_['column_date_added']  = 'Ngày thêm';
$_['column_customer']    = 'Khách hàng';
$_['column_product']     = 'Tên sản phẩm';
$_['column_model']       = 'Mẫu';
$_['column_quantity']    = 'Số lượng';
$_['column_price']       = 'Giá bán';
$_['column_opened']      = 'Khai trương';
$_['column_comment']     = 'Bình luận';
$_['column_reason']      = 'Lý do';
$_['column_action']      = 'Hoạt động';

// Entry
$_['entry_order_id']     = 'ID đơn hàng';
$_['entry_date_ordered'] = 'Ngày đặt hàng';
$_['entry_firstname']    = 'Tên';
$_['entry_lastname']     = 'Họ';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Điện thoại';
$_['entry_product']      = 'Tên sản phẩm';
$_['entry_model']        = 'Mã sản phẩm';
$_['entry_quantity']     = 'Số lượng';
$_['entry_reason']       = 'Lý do trả hàng';
$_['entry_opened']       = 'Sản phẩm được mở';
$_['entry_fault_detail'] = 'Bị lỗi hoặc chi tiết khác';

// Error
$_['text_error']         = 'Các trả về cho bạn yêu cầu không thể được tìm thấy!';
$_['error_order_id']     = 'ID đơn hàng là bắt buộc!';
$_['error_firstname']    = 'Tên phải được từ 1 đến 32 ký tự!';
$_['error_lastname']     = 'Họ phải có từ 1 đến 32 ký tự!';
$_['error_email']        = 'E-Mail Địa chỉ không có vẻ hợp lệ!';
	$_['error_telephone']    = 'Điện thoại phải có từ 3 đến 32 ký tự!';
$_['error_product']      = 'Tên sản phẩm phải lớn hơn 3 và nhỏ hơn 255 ký tự!';
$_['error_model']        = 'Model sản phẩm phải lớn hơn 3 và nhỏ hơn 64 ký tự!';
$_['error_reason']       = 'Bạn phải chọn một lí do sản phẩm trở lại!';
$_['error_agree']        = 'Cảnh báo: Bạn phải đồng ý với %s!';
