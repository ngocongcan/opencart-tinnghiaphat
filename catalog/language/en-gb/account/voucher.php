<?php
// Heading
$_['heading_title']    = 'Đặt mua phiếu quà tặng';

// Text
$_['text_account']     = 'Tài khoản';
$_['text_voucher']     = 'Phiếu quà tặng';
$_['text_description'] = 'Phiếu quà tặng này sẽ được gửi đến người nhận sau khi bạn thanh toán cho.';
$_['text_agree']       = 'Tôi biết rằng phiếu quà tặng không thể hoàn lại.';
$_['text_message']     = '<p>Cảm ơn bạn đã mua một món quà! Một khi bạn đã hoàn thành đặt hàng của bạn món quà chứng nhận của bạn sẽ được gửi một e-mail với các chi tiết làm thế nào để chuộc lại giấy chứng nhận món quà của họ.</p>';
$_['text_for']         = '%s Giấy chứng nhận quà tặng cho %s';

// Entry
$_['entry_to_name']    = 'Tên người nhận';
$_['entry_to_email']   = 'Email người nhận';
$_['entry_from_name']  = 'Tên của bạn';
$_['entry_from_email'] = 'Email của bạn';
$_['entry_theme']      = 'Chủ đề phiếu quà tặng';
$_['entry_message']    = 'Thông điệp';
$_['entry_amount']     = 'Số tiền';

// Help
$_['help_message']     = 'Không bắt buộc';
$_['help_amount']      = 'Giá trị phải từ %s và %s';

// Error
$_['error_to_name']    = 'Tên Người nhận phải có từ 1 đến 64 ký tự!';
$_['error_from_name']  = 'Tên của bạn phải có từ 1 đến 64 ký tự!';
$_['error_email']      = 'Địa chỉ E-Mail không có vẻ hợp lệ!';
$_['error_theme']      = 'Bạn phải chọn một chủ đề!';
$_['error_amount']     = 'Số tiền phải là giữa %s và %s!';
$_['error_agree']      = 'Cảnh báo: Bạn phải đồng ý rằng phiếu quà tặng không hoàn lại!';
