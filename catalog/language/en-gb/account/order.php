<?php
// Heading
$_['heading_title']         = 'Lịch sử đơn hàng';

// Text
$_['text_account']          = 'Tài khoản';
$_['text_order']            = 'Thông tin đặt hàng';
$_['text_order_detail']     = 'Đặt hàng Chi tiết';
$_['text_invoice_no']       = 'Hóa đơn số:';
$_['text_order_id']         = 'Mã đơn hàng:';
$_['text_date_added']       = 'Ngày thêm:';
$_['text_shipping_address'] = 'Địa chỉ giao hàng';
$_['text_shipping_method']  = 'Phương pháp vận chuyển:';
$_['text_payment_address']  = 'Địa chỉ thanh toán';
$_['text_payment_method']   = 'Phương thức thanh toán:';
$_['text_comment']          = 'Bình luận';
$_['text_history']          = 'Lịch sử đơn hàng';
$_['text_success']          = 'Thành công: Bạn đã thêm <a href="%s">%s</a><a href="%s">vào giỏ hàng của bạn</a>!';
$_['text_empty']            = 'Bạn đã không thực hiện bất kỳ đơn đặt hàng trước!';
$_['text_error']            = 'Đơn hàng bạn yêu cầu không thể được tìm thấy!';

// Column
$_['column_order_id']       = 'Mã đơn hàng';
$_['column_customer']       = 'Khách hàng';
$_['column_product']        = 'Số sản phẩm';
$_['column_name']           = 'Tên sản phẩm';
$_['column_model']          = 'Mẫu';
$_['column_quantity']       = 'Số lượng';
$_['column_price']          = 'Giá';
$_['column_total']          = 'Tổng số';
$_['column_action']         = 'Hoạt động';
$_['column_date_added']     = 'Ngày thêm';
$_['column_status']         = 'Trạng thái';
$_['column_comment']        = 'Bình luận';

// Error
$_['error_reorder']         = '%s hiện không có sẵn để được sắp xếp lại.';