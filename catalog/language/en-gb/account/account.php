<?php
// Heading
$_['heading_title']      = 'Tài khoản của tôi';

// Text
$_['text_account']       = 'Tài khoản';
$_['text_my_account']    = 'Tài khoản của tôi';
$_['text_my_orders']     = 'Đơn hàng của tôi';
$_['text_my_newsletter'] = 'Bản tin';
$_['text_edit']          = 'Chỉnh sửa thông tin tài khoản';
$_['text_password']      = 'Thay đổi mật khẩu';
$_['text_address']       = 'Chỉnh sửa địa chỉ';
$_['text_credit_card']   = 'Chỉnh sửa thông tin thẻ tín dụng';
$_['text_wishlist']      = 'Sửa đổi danh sách mong muốn của bạn';
$_['text_order']         = 'Xem lịch sử đặt hàng của bạn';
$_['text_download']      = 'Tải về';
$_['text_reward']        = 'Điểm thưởng';
$_['text_return']        = 'Xem yêu cầu đổi trả của bạn';
$_['text_transaction']   = 'Giao dịch của bạn';
$_['text_newsletter']    = 'Theo dõi / hủy đăng ký nhận bản tin';
$_['text_recurring']     = 'Thanh toán định kỳ';
$_['text_transactions']  = 'Giao dịch';