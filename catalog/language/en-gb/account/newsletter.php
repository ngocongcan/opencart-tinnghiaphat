<?php
// Heading
$_['heading_title']    = 'Bản tin đăng ký';

// Text
$_['text_account']     = 'Tài khoản';
$_['text_newsletter']  = 'Bản tin';
$_['text_success']     = 'Thành công: Thuê bao nhận bản tin của bạn đã được cập nhật thành công!';

// Entry
$_['entry_newsletter'] = 'Theo dõi';
