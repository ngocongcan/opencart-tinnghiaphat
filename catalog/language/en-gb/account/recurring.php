<?php
// Heading
$_['heading_title']           = 'Thanh toán định kỳ';

// Text
$_['text_account']            = 'Tài khoản';
$_['text_recurring']          = 'Thông tin Thanh toán định kỳ';
$_['text_recurring_detail']   = 'Định kỳ Chi tiết Thanh toán';
$_['text_order_recurring_id'] = 'ID định kỳ:';
$_['text_date_added']         = 'Ngày thêm:';
$_['text_status']             = 'Trạng thái:';
$_['text_payment_method']     = 'Phương thức thanh toán:';
$_['text_order_id']           = 'Mã yêu cầu:';
$_['text_product']            = 'Sản phẩm:';
$_['text_quantity']           = 'Số lượng:';
$_['text_description']        = 'Mô tả';
$_['text_reference']          = 'Tài liệu tham khảo';
$_['text_transaction']        = 'Giao dịch';


$_['text_status_1']           = 'Hoạt động';
$_['text_status_2']           = 'Không hoạt động';
$_['text_status_3']           = 'Hủy';
$_['text_status_4']           = 'Hoãn lại';
$_['text_status_5']           = 'Đã hết hạn';
$_['text_status_6']           = 'Chưa giải quyết';

$_['text_transaction_date_added'] = 'Tạo';
$_['text_transaction_payment'] = 'Thanh toán';
$_['text_transaction_outstanding_payment'] = 'Thanh toán thành công';
$_['text_transaction_skipped'] = 'Thanh toán bỏ qua';
$_['text_transaction_failed'] = 'Thanh toán không thành công';
$_['text_transaction_cancelled'] = 'Hủy';
$_['text_transaction_suspended'] = 'Hoãn lại';
$_['text_transaction_suspended_failed'] = 'Bị treo';
$_['text_transaction_outstanding_failed'] = 'Thanh toán thất bại';
$_['text_transaction_expired'] = 'Đã hết hạn';




$_['text_empty']                 = 'Không thanh toán định kỳ tìm thấy!';
$_['text_error']                 = 'Trình tự định kỳ bạn yêu cầu không thể được tìm thấy!';








$_['text_cancelled'] = 'Recurring payment has been cancelled';

// Column
$_['column_date_added']         = 'Date Added';
$_['column_type']               = 'Type';
$_['column_amount']             = 'Amount';
$_['column_status']             = 'Status';
$_['column_product']            = 'Product';
$_['column_order_recurring_id'] = 'Recurring ID';

// Error
$_['error_not_cancelled'] = 'Error: %s';
$_['error_not_found']     = 'Could not cancel recurring';

// Button
$_['button_return']       = 'Trở về';
