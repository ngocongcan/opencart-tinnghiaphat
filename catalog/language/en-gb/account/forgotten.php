<?php
// Heading
$_['heading_title']   = 'Quên mật khẩu?';

// Text
$_['text_account']    = 'Tài khoản';
$_['text_forgotten']  = 'Quên mật khẩu';
$_['text_your_email'] = 'Địa chỉ email của bạn';
$_['text_email']      = 'Nhập địa chỉ e-mail liên kết với tài khoản của bạn. Nhấp vào trình có liên kết đặt lại mật khẩu e-mail đến bạn.';
$_['text_success']    = 'Một email với một liên kết xác nhận đã được gửi địa chỉ email của bạn.';

// Entry
$_['entry_email']     = 'Địa chỉ email';
$_['entry_password']  = 'Mật khẩu mới';
$_['entry_confirm']   = 'Xác nhận';

// Error
$_['error_email']     = 'Cảnh báo: E-Mail Địa chỉ không được tìm thấy trong hồ sơ của chúng tôi, xin vui lòng thử lại!';
$_['error_approved']  = 'Cảnh báo: Tài khoản của bạn đòi hỏi phải có sự chấp thuận trước khi bạn có thể đăng nhập.';
$_['error_password']  = 'Mật khẩu phải từ 4 đến 20 ký tự!';
$_['error_confirm']   = 'Mật khẩu và xác nhận mật khẩu không phù hợp!';
