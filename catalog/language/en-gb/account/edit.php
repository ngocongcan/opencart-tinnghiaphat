<?php
// Heading
$_['heading_title']      = 'Thông tin Tài khoản của tôi';

// Text
$_['text_account']       = 'Tài khoản';
$_['text_edit']          = 'Chỉnh sửa thông tin';
$_['text_your_details']  = 'Thông tin cá nhân của bạn';
$_['text_success']       = 'Thành công: Tài khoản của bạn đã được cập nhật thành công.';

// Entry
$_['entry_firstname']    = 'Tên';
$_['entry_lastname']     = 'Họ';
$_['entry_email']        = 'E-Mail';
$_['entry_telephone']    = 'Điện thoại';
$_['entry_fax']          = 'Fax';

// Error
$_['error_exists']                = 'Cảnh báo: Địa chỉ E-Mail đã được đăng ký!';
$_['error_firstname']             = 'Tên phải được từ 1 đến 32 ký tự!';
$_['error_lastname']              = 'Họ phải có từ 1 đến 32 ký tự!';
$_['error_email']                 = 'E-Mail Địa chỉ không có vẻ hợp lệ!';
$_['error_telephone']             = 'Điện thoại phải có từ 3 đến 32 ký tự!';
$_['error_custom_field']          = '%s là bắt buộc!';