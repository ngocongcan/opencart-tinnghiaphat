<?php
// Heading
$_['heading_title']  = 'Liên hệ chúng tôi';

// Text
$_['text_location']  = 'Địa điểm của chúng tôi';
$_['text_store']     = 'Cửa hàng của chúng tôi';
$_['text_contact']   = 'Mẫu liên hệ';
$_['text_address']   = 'Địa chỉ';
$_['text_telephone'] = 'Điện thoại';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Giờ mở cửa';
$_['text_comment']   = 'Nhận xét';
$_['text_success']   = '<p>Yêu cầu của bạn đã được gửi thành công đến các chủ cửa hàng!</p>';

// Entry
$_['entry_name']     = 'Tên của bạn';
$_['entry_email']    = 'Địa chỉ email';
$_['entry_enquiry']  = 'Yêu cầu';

// Email
$_['email_subject']  = 'Yêu cầu %s';

// Errors
$_['error_name']     = 'Tên phải có từ 3 đến 32 ký tự!';
$_['error_email']    = 'Địa chỉ E-Mail không có vẻ hợp lệ!';
$_['error_enquiry']  = 'Yêu cầu phải có từ 10 đến 3000 ký tự!';
