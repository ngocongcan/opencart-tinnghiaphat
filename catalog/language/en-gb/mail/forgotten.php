<?php
// Text
$_['text_subject']  = '%s - Yêu cầu đặt lại mật khẩu';
$_['text_greeting'] = 'Mật khẩu mới đã được yêu cầu cho %s tài khoản của khách hàng.';
$_['text_change']   = 'Để thiết lập lại mật khẩu của bạn bấm vào liên kết dưới đây:';
$_['text_ip']       = 'Các IP được sử dụng để thực hiện yêu cầu này là: %s';
