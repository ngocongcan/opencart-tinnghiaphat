<?php
// Text
$_['text_subject']		        = '%s - Chương trình liên kết';
$_['text_welcome']		        = 'Cảm ơn bạn đã tham gia chương trình liên kết của %s!';
$_['text_login']                = 'Tài khoản của bạn hiện đã được tạo ra và bạn có thể đăng nhập bằng cách sử dụng địa chỉ e-mail và mật khẩu của bạn bằng cách truy cập trang web của chúng tôi hoặc tại URL sau:';
$_['text_approval']		        = 'Tài khoản của bạn phải được chấp thuận trước khi bạn có thể đăng nhập. Khi được chấp nhận, bạn có thể đăng nhập bằng cách sử dụng địa chỉ e-mail và mật khẩu của bạn bằng cách truy cập trang web của chúng tôi hoặc tại URL sau:';
$_['text_services']		        = 'Sau khi đăng nhập, bạn sẽ có thể tạo mã theo dõi, các khoản hoa hồng theo dõi và chỉnh sửa thông tin tài khoản của bạn.';
$_['text_thanks']		        = 'Cảm ơn,';
$_['text_new_affiliate']        = 'Chương trình liên kết mới';
$_['text_signup']		        = 'Một liên kết mới đã đăng ký:';
$_['text_store']		        = 'Cửa hàng:';
$_['text_firstname']	        = 'Tên:';
$_['text_lastname']		        = 'Họ:';
$_['text_company']		        = 'Công ty:';
$_['text_email']		        = 'E-Mail:';
$_['text_telephone']	        = 'Điện thoại:';
$_['text_website']		        = 'Trang mạng:';
$_['text_order_id']             = 'Mã đơn hàng:';
$_['text_transaction_subject']  = '%s - Ủy ban liên kết';
$_['text_transaction_received'] = 'Bạn đã nhận được hoa hồng %s!';
$_['text_transaction_total']    = 'Tổng số tiền hoa hồng của bạn hiện tại là %s.';
