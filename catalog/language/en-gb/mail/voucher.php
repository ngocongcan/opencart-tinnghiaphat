<?php
// Text
$_['text_subject']  = 'Bạn đã gửi một món quà từ %s';
$_['text_greeting'] = 'Xin chúc mừng, bạn đã nhận được một giá trị Giấy chứng nhận quà tặng %s';
$_['text_from']     = 'Giấy chứng nhận quà tặng này đã được gửi đến cho bạn %s';
$_['text_message']  = 'Với một tin nhắn nói';
$_['text_redeem']   = 'To redeem this Gift Certificate, write down the redemption code which is <b>%s</b> then click on the the link below and purchase the product you wish to use this gift certificate on. You can enter the gift certificate code on the shopping cart page before you click checkout.';
$_['text_footer']   = 'Hãy trả lời e-mail nếu bạn có bất kỳ câu hỏi.';
