<?php
// Text
$_['text_captcha'] = 'Captcha';

// Entry
$_['entry_captcha'] = 'Nhập mã kiểm tra vào ô bên dưới';

// Error
$_['error_captcha'] = 'Mã xác nhận không phù hợp với hình ảnh!';
