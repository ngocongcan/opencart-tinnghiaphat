<?php
// Heading
$_['heading_title']    = 'Affiliate';

// Text
$_['text_register']    = 'Ghi danh';
$_['text_login']       = 'Đăng nhập';
$_['text_logout']      = 'Đăng xuất';
$_['text_forgotten']   = 'Đã quên mật khẩu';
$_['text_account']     = 'Tài khoản của tôi';
$_['text_edit']        = 'Chỉnh sửa tài khoản';
$_['text_password']    = 'Mật khẩu';
$_['text_payment']     = 'Payment Options';
$_['text_tracking']    = 'Affiliate Tracking';
$_['text_transaction'] = 'Giao dịch';
