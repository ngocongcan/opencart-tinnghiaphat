<?php
class ControllerExtensionModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/featured');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);
				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}
					$categories = array();
					$category = "";
					$mainCategory = "";
					if($product_info['categories']){
						$categories = explode("|",$product_info['categories']);
						if(count($categories) > 0){
							$category = $categories[count($categories) - 1];
							$mainCategory = $categories[0];
						}
					}

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'categories'  => $categories,
						'category'  => $category,
						'maincategory'  => $mainCategory,
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'manufacturer'        => $product_info['manufacturer'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}


		$products = $data['products'];
		usort($products, function($a, $b) {
			return strcmp($a['manufacturer'], $b['manufacturer']);
		});
		$groupProducts = array();
		foreach($products as $prod){
			if(isset($groupProducts[$prod['maincategory']]) == null){
				$groupProducts[$prod['maincategory']] = array();
			}
			if(isset($groupProducts[$prod['maincategory']][$prod['manufacturer']]) == null){
				$groupProducts[$prod['maincategory']][$prod['manufacturer']] = array();
			}
			$groupProducts[$prod['maincategory']][$prod['manufacturer']][] = $prod;
		}

		$data['group_products'] = $groupProducts;

		if ($data['products']) {
			return $this->load->view('extension/module/featured', $data);
		}
	}
}