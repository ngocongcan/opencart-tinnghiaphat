<?php
// HTTP
define('HTTP_SERVER', 'http://tinnghiaphat.local/admin/');
define('HTTP_CATALOG', 'http://tinnghiaphat.local/');

// HTTPS
define('HTTPS_SERVER', 'http://tinnghiaphat.local/admin/');
define('HTTPS_CATALOG', 'http://tinnghiaphat.local/');

// DIR
define('DIR_APPLICATION', '/Users/canngo/Documents/www/tinnghiaphat/admin/');
define('DIR_SYSTEM', '/Users/canngo/Documents/www/tinnghiaphat/system/');
define('DIR_IMAGE', '/Users/canngo/Documents/www/tinnghiaphat/image/');
define('DIR_LANGUAGE', '/Users/canngo/Documents/www/tinnghiaphat/admin/language/');
define('DIR_TEMPLATE', '/Users/canngo/Documents/www/tinnghiaphat/admin/view/template/');
define('DIR_CONFIG', '/Users/canngo/Documents/www/tinnghiaphat/system/config/');
define('DIR_CACHE', '/Users/canngo/Documents/www/tinnghiaphat/system/storage/cache/');
define('DIR_DOWNLOAD', '/Users/canngo/Documents/www/tinnghiaphat/system/storage/download/');
define('DIR_LOGS', '/Users/canngo/Documents/www/tinnghiaphat/system/storage/logs/');
define('DIR_MODIFICATION', '/Users/canngo/Documents/www/tinnghiaphat/system/storage/modification/');
define('DIR_UPLOAD', '/Users/canngo/Documents/www/tinnghiaphat/system/storage/upload/');
define('DIR_CATALOG', '/Users/canngo/Documents/www/tinnghiaphat/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'tnp');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
